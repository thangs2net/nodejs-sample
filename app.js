﻿
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var cors = require('express-cors');

var expressValidator = require('express-validator');
var bodyParser = require('body-parser');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.bodyParser());
app.use(expressValidator());

// add angular js to proj
app.use('/bower_components', express.static(__dirname + "/bower_components"));
app.use('/node_modules', express.static(__dirname + "/node_modules"));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);


//app.use(cors({
//    allowedOrigins: [
//        'github.com', 'google.com', 'feeder.thangs2.net', 'dev.lixido.com'
//    ]
//}));


var brandRoute = require('./routes/brandRouteConfig.js');
new brandRoute(app);

var productRoute = require('./routes/productRouteConfig.js');
new productRoute(app);

var userRoute = require('./routes/userRouteConfig.js');
new userRoute(app);

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
