﻿(function () {
    'user strict'
    
    angular.module("userModule.userService", []).factory("userService", userService);
    
    // $inject
    userService.$inject = ["$http", "$q"];
    
    function userService($http, $q) {

        var canceler = $q.defer();
        var userApi = {};
        
        userApi.createUser = function (user) {
            return $http.put("/createUser", user, { timeout: canceler.promise });
        };
        
        userApi.checkExistedUserByIdAsync = function (user) {
            return $http.post("/checkExistedUserByIdAsync", user, { timeout: canceler.promise });
        };
        
        userApi.checkExistedUserByEmailAsync = function (user) {
            return $http.post("/checkExistedUserByEmailAsync", user, { timeout: canceler.promise });
        };

        userApi.abort = function () {
            canceler.resolve();
            canceler = $q.defer();
        }
        
        return userApi;

    }

})();