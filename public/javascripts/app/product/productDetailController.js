﻿(function () {
    'user strict'

    angular.module("productModule").controller("productDetailController", productDetailController);
    
    // $inject
    productDetailController.$inject = ["productService", "$scope", "$q"];
    
    function productDetailController(productService, $scope, $q) {
        
        $scope.product = {
            id : "",
            productName: "",
            description: "",
            price: 0,
            color: "",
            createdDate: new Date(),
            availableStatus: [],
            brandId: 0,
            rating: 5,
            //reviews: {}
        };
        
        $scope.message = {
            
            containsSuccessfulMessage : false,
            successfulMessage : ""

        };
        
        $scope.viewProductBinding = function (product) {

            $scope.product.id = product.productId;
            $scope.product.productName = product.productName;
            $scope.product.description = product.description;
            $scope.product.price = product.price;
            $scope.product.color = product.color;
            $scope.product.createdDate = product.createdDate;
            $scope.product.availableStatus = product.availableStatus;
            $scope.product.brandId = product.brandId;
            
            if (product.rating == null) {
                $scope.product.rating = 5;
            }else{ $scope.product.rating = product.rating;}
        }
        
        $scope.init = function (products) {
            
            if (products[0] && products[0].length > 0) {

                $scope.viewProductBinding(products[0][0]);

            }
            else {

                $scope.message.containsSuccessfulMessage = true;
                $scope.message.successfulMessage = "No product found. \n " +
                                "Please check valid Url or send existed product.";
                
            }

        };
    

    }
})()