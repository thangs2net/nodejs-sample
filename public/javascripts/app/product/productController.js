﻿(function () {
    'user strict'

    angular.module("productModule").controller("productController", productController);

    // $inject
    productController.$inject = ["productService", "$scope", "$q"];

    function productController(productService, $scope, $q) {

        $scope.products = [];
        

        $scope.viewProductsBinding = function (products) {

            products.forEach(function (product) {

                $scope.products.push({
                    id: product.Id,
                    productName: product.ProductName,
                    description: product.Description,
                    price: product.Price,
                    color: product.Color,
                    createdDate: product.CreatedDate,
                    availableStatus: product.AvailableStatus,
                    brandId: product.BrandId,
                });

            });

        }

        $scope.init = function (products) {

            if (products) {
                //$scope.products = products;
                $scope.viewProductsBinding(products);
            }

        };

        $scope.getProductsByBrandIds = function () {

            productService.getProductsByBrandIds(brandIds)
                .then(function (data) {

                    console.log("data");
                    console.log(data);

                })
            .error(function (data) {

                console.log("data");
                console.log(data);

            });

        }
        
        

    }
})()