﻿(function () {
    'user strict'
    
    angular.module("productModule").controller("productReviewController", productReviewController);
    
    // $inject
    productReviewController.$inject = [
        "$scope", "$q", "$timeout", "requiredFieldValidationService", "userService", "productService"];
    
    function productReviewController(
        $scope,
        $q,
        $timeout,
        requiredFieldValidationService,
        userService,
        productService) 
    {
        $scope.userReviews = [];

        // default rating is 5
        $scope.review = {
            
            rating: 5,
            email: "",
            comment: "",
            //productId: 0
        };
        
        $scope.validationResult = {
            
            containsValidationError : false,
            validationSummary : ""

        };
        $scope.message = {
            
            containsSuccessfulMessage : false,
            successfulMessage : ""

        };
        $scope.displayMessage = function () {
            
            $scope.message.containsSuccessfulMessage = true;
            $scope.message.successfulMessage = "Review has been added.";

        };
        $scope.clearMessage = function () {
            
            $scope.message.containsSuccessfulMessage = false;
            $scope.message.successfulMessage = "";

        };
        $scope.clearReview = function () {
            
            $scope.review.rating = 5;
            $scope.review.email = "";
            $scope.review.comment = "";

        };
        

        getReview();
        function reviewBinding(reviews){

            if (reviews && reviews.length > 0) {
                console.log(reviews.length);
                reviews.forEach(function (review) { 
                    
                    $scope.userReviews.push({
                        email: review.email,
                        comment: review.comment,
                    });

                });
            }

            console.log("foreach");
            console.log($scope.userReviews);

        }
        function getReview(){
            
            // get id endpoint
            var endpoint = productService.getEndPoint();
            if (endpoint == 'underfined') {
                endpoint = 0;
            }
            $scope.review.productId = endpoint;

            productService.getReviewsByProductId($scope.review.productId).then(function (data) {
                
                var reviews = data.data.reviews;
                reviewBinding(reviews);

            });
        }

        $scope.checkExistedUserByEmail = function(email) {
            
            var user = {
                username: "anonymous",
                email: email,
                userType: 1,
                //dateOfBirth: new Date()
            };
            var deferred = $q.defer();
            
            // i will solve promise hell later
            userService.checkExistedUserByEmailAsync(user).then(function (data) {
                
                var result = data.data;

                // existed user
                if (result && result.existed === true) {
                    
                    var userId = result.results[0].Id;
                    
                    deferred.resolve(userId);

                }
                else if (result && result.existed === false) {
                    
                    // not exist user, so we try to create new user, such as anonymous user
                    userService.createUser(user).then(function (data) {
                        
                        var result = data.data.result;

                        var userId = result.insertId;
                        deferred.resolve(userId);

                    });

                }

            });

            return deferred.promise;
        }

        $scope.createProductReview = function (review) {

            var validationMessages = requiredFieldValidationService.getRequiredFieldValidationErrorMessage(
                [
                    { name: $scope.review.email || "" , errorMessage: "Please enter email.\n" },
                    { name: $scope.review.comment || "" , errorMessage: "Please enter your comments.\n" }
                ]);

            if (validationMessages.length > 0) {
                
                $scope.validationResult.containsValidationError = true;
                
                angular.element('#validationErrorMessage').empty();
                validationMessages.forEach(function (errorMessage) {
                    
                    angular.element('<li></li>')
                            .html(errorMessage)
                            .appendTo('#validationErrorMessage');

                });

            }
            else {

                $scope.validationResult.containsValidationError = false;

                $scope.checkExistedUserByEmail(review.email).then(function (data) { 

                    if (data && data > 0) {

                        // create review
                        review.userId = data;

                        productService.createReview(review).then(function (data) { 
                        
                            console.log("create review data");
                            console.log(data);

                            //$scope.message.containsSuccessfulMessage = true;
                            //$scope.message.successfulMessage = "Review has been saved.";
                            
                            $scope.displayMessage();
                            $timeout(function afterTimeout() {
                                $scope.clearMessage();
                                $scope.clearReview();
                            }, 5000);    

                        });

                    }
                    else {

                        
                        $scope.validationResult.containsValidationError = true;
                        
                        angular.element('#validationErrorMessage').empty();
                        angular.element('<li></li>')
                            .html("Please check data.")
                            .appendTo('#validationErrorMessage');

                    }

                });

            }
        }

    }

})();