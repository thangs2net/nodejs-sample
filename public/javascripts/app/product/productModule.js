﻿(function () {
    'user strict'
    
    angular.module('productModule', 
        [
        "productModule.productService",
        "validationModule",
        "componentModule.componentDirectives",
        "userModule",
        "angularUtils.directives.dirPagination"
    ]);

    
})();