﻿(function () {
    'user strict'
    
    angular.module("productModule.productService", []).factory("productService", productService);
    
    // $inject
    productService.$inject = ["$http", "$q", "$location"];
    
    function productService($http, $q, $location) {
        
        var canceler = $q.defer();
        var productApi = {};
        
        // get API
        productApi.getProductsByBrandId = function (brandId) {
            return $http.get("/products/" + brandId + "/brand", { timeout: canceler.promise });
        }
        
        productApi.getProductById = function (id) {

            return $http.get("/product/" + id, { timeout: canceler.promise });

        }
        
        productApi.getProductsByFilterAysnc = function (paramFilter) {
            
            return $http.post("/productFilter", paramFilter, { timeout: canceler.promise });

        }
        
        // product review
        productApi.createReview = function (review) {
            return $http.put("/createReview", review, { timeout: canceler.promise });
        };
        
        productApi.getReviewsByProductId = function (productId) {
            return $http.get("/reviews/"+ productId + "/product", { timeout: canceler.promise });
        }

        productApi.getEndPoint = function () {
            
            var absoluteUrl = $location.absUrl();
            
            var segment = absoluteUrl.split('/');
            
            var endpoint = segment[segment.length - 1];
            
            var rs = 0;
            try {
                rs = parseInt(endpoint);
            }
            catch (e) { }
            
            return rs;
        }
        
        productApi.abort = function () {
            canceler.resolve();
            canceler = $q.defer();
        }
        
        return productApi;
    }

})();