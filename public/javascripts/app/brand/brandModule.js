﻿(function () {
    'user strict'

    var brandModule = angular.module('brandModule', 
        [
        "brandModule.brandService", 
        "validationModule"
    ]);
})()