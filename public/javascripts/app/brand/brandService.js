﻿(function () {
    'user strict'
    
    angular.module("brandModule.brandService", []).factory("brandService", brandService);
    
    brandService.$inject = ["$http", "$location", "$q"];
    
    function brandService($http, $location, $q) {
        
        var canceler = $q.defer();
        var brandApi = {};
        
        // 
        brandApi.getEndPoint = function () {
            
            var absoluteUrl = $location.absUrl();
            
            var segment = absoluteUrl.split('/');
            
            var endpoint = segment[segment.length - 1];
            
            var rs = 0;
            try {
                rs = parseInt(endpoint);
            }
            catch (e) { }
            
            return rs;
        }
        
        // create API
        brandApi.createBrand = function (brand) {
            return $http.put("/createBrand", brand, { timeout: canceler.promise });
        };
        
        // get API
        brandApi.getBrands = function (take) {
            return $http.get("/getBrands/" + take, { timeout: canceler.promise });
            //var data = $http.get("http://feeder.thangs2.net/api/NewsFeeder");
            //console.log("server ==== server");
            //console.log(data);

            //return data;
        }
        
        brandApi.getApi = function (take) {
            console.log("get api ....");
            return $http.get("/getApi/", { timeout: canceler.promise });
        }
        
        
        brandApi.abort = function () {
            canceler.resolve();
            canceler = $q.defer();
        }
        
        return brandApi;
    }
})();