﻿(function () {
    'user strict'
    
    angular.module("brandModule").controller("createBrandController", createBrandController);
    
    createBrandController.$inject = [
        "$scope", 
        "$timeout", 
        "brandService", 
        "requiredFieldValidationService"];
    
    function createBrandController(
        $scope, 
        $timeout, 
        brandService, 
        requiredFieldValidationService) {
        
        $scope.brand = {
            name: "",
            description: ""
        }

        $scope.validationResult = {
            
            containsValidationError : false,
            validationSummary : ""

        };
        $scope.message = {
            
            containsSuccessfulMessage : false,
            successfulMessage : ""

        };
        $scope.displayMessage = function () {

            $scope.message.containsSuccessfulMessage = true;
            $scope.message.successfulMessage = "Brand has been saved.";

        };
        $scope.clearMessage = function () {

            $scope.message.containsSuccessfulMessage = false;
            $scope.message.successfulMessage = "";

        };
        $scope.clearBrand = function () {

            $scope.brand.name = "";
            $scope.brand.description = "";

        };

        $scope.createBrand = function (brand) {

            var validationMessages = requiredFieldValidationService.getRequiredFieldValidationErrorMessage(
                [
                    { name: $scope.brand.name || "" , errorMessage: "Please enter brand name.\n"},
                    { name: $scope.brand.description || "" , errorMessage: "Please enter brand description.\n" }
                ]);
            
            
            if (validationMessages.length > 0) {

                $scope.validationResult.containsValidationError = true;
                
                angular.element('#validationErrorMessage').empty();
                validationMessages.forEach(function (errorMessage) { 
                    
                    angular.element('<li></li>')
                            .html(errorMessage)
                            .appendTo('#validationErrorMessage');

                });

            }
            else {
                
                $scope.validationResult.containsValidationError = false;
                brandService.createBrand(brand)
                    .success(function (data) {

                    if (data.status && data.status === "successful") {
                        
                        $scope.message.containsSuccessfulMessage = true;
                        $scope.message.successfulMessage = "Brand has been saved.";

                        //$scope.displayMessage();
                        $timeout(function afterTimeout() {
                            $scope.clearMessage();
                            $scope.clearBrand();
                        }, 5000);    

                    }
                    else if (data.status && data.status === "error") {

                        console.log(data.errorMessage);
                        $scope.validationResult.containsValidationError = true;
                        
                        angular.element('#validationErrorMessage').empty();
                        angular.element('<li></li>')
                            .html(data.errorMessage)
                            .appendTo('#validationErrorMessage');

                    }
                });

            }

        }
    }
})()