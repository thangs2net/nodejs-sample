﻿(function () {
    'user strict'
    
    angular.module("brandModule").controller("brandController", brandController);
    
    brandController.$inject = ["$scope", "brandService", "$timeout"];
    
    function brandController($scope, brandService, $timeout) {
        
        $scope.brands = [];
        
        getBrands();
        
        function bindView(brands) {
            
            brands.forEach(function (brand) {
                $scope.brands.push({
                    name: brand.Name,
                    description: brand.Description,
                    id: brand.Id
                })
            });

        }
        
        function getBrands() {
            
            var endpoint = brandService.getEndPoint();
            if (endpoint == 'underfined') {
                endpoint = 0;
            }
            
            brandService.getBrands(endpoint)
                .success(function (data) {
                
                console.log("data");
                console.log(data);
                
                if (data 
                        && data.brands 
                        && data.brands.length > 0) {
                    //$scope.brands = data.brands;
                    bindView(data.brands);
                }

            });

        }
    }

})();