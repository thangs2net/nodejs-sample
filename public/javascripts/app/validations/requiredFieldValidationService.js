﻿(function () {
    'user strict'
    
    angular.module("validationModule.validationService",[])
    .factory("requiredFieldValidationService", requiredFieldValidationService);
    
    
    function requiredFieldValidationService() {
        
        function _getRequiredValidationMessage(requiredInfos) {
            var errorMessages = [];
            if (requiredInfos && requiredInfos.length > 0) {
                
                requiredInfos.forEach(function (requiredInfo) {
                    
                    if (requiredInfo.name !== "underfined" 
                    &&
                    (requiredInfo.name === null 
                        || requiredInfo.name == '' 
                        || requiredInfo.length > 0)) {
                        
                        errorMessages.push(requiredInfo.errorMessage);

                    }

                });

            }
            
            return errorMessages;
        };
        
        return {
            getRequiredFieldValidationErrorMessage: _getRequiredValidationMessage
        };
    }

})();

