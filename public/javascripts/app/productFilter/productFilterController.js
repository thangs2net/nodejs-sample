﻿(function () {
    'user strict'
    
    angular.module("productFilterModule").controller("productFilterController", productFilterController);
    
    // $inject
    productFilterController.$inject = ["$scope", "$timeout", "brandService", "productService", "$http"];
    
    function productFilterController($scope, $timeout, brandService, productService, $http) {
        
        // get data from APIs
        $scope.apiCategoryList = function () {
            var config = {
                headers: {
                    //'outlet_id': 8956,
                    //'token': '5911E94579B9965C9B49733C099A70B5'
                    'Accept' : 'application/json'
                }
            };
            //var url = "http://dev.lixido.com/productservice/json/category_sel_all";
            //$http.get(url, config).success(function (data) {
            //    console.log("============== data ===============");
            //    console.log(data);
            //});

            var url = "http://feeder.thangs2.net/api/NewsFeeder";
            $http.get(url, config).success(function (data) {
                console.log("============== data ===============");
                console.log(data);
            });
        }
        
        $scope.init = function() {
            //$scope.apiCategoryList();
        }
        $scope.init();

        $scope.brands = [];
        
        $scope.selectedBrands = [];
        
        $scope.products = [];
        
        $scope.pagination = {
            itemsPerPage : 9,
            current: 1,
            totalItems: 9
        };
        
        $scope.paramFilter = {
            brandIds : $scope.selectedBrands,
            pageIndex : $scope.pagination.current,
            itemsPerPage : $scope.pagination.itemsPerPage
        }
        
        //$scope.init();
        getBrands();
        
        
        $scope.viewProductsBinding = function (products) {
            
            $scope.products = [];
            products.forEach(function (product) {
                
                $scope.products.push({
                    id: product.productId,
                    productName: product.productName,
                    description: product.description,
                    price: product.price,
                    color: product.color,
                    createdDate: product.createdDate,
                    availableStatus: product.availableStatus,
                    brandId: product.brandId,

                    totalItems: product.totalItems
                });

            });
            
        }
        
        $scope.viewBrandBinding = function (brands) {
            
            brands.forEach(function (brand) {
                // for brands
                $scope.brands.push({
                    name: brand.Name,
                    description: brand.Description,
                    id: brand.Id,
                    
                    selected: true
                });
                
                // for ids
                //$scope.selectedBrands.push({ id: brand.Id, name: brand.Name, seleted: true });
                //$scope.selectedBrands.push({ id: brand.Id});
            });

        }
        
        $scope.populatePaging = function (totalItems){

            $scope.pagination.totalItems = totalItems;

            console.log("$scope.pagination.totalItems");
            console.log($scope.pagination.totalItems);
        }

        function getBrands() {


            brandService.getBrands()
                .success(function(data) {

                        if (data
                            && data.brands
                            && data.brands.length > 0) {

                            $scope.viewBrandBinding(data.brands);
                        }

                    }
                ).error(function(err) {
                    console.log("err");
                    console.log(err);
                });
        }
        
        function getProducts() {
            
            //var brandIds = $scope.selectedBrands;
            
            

            productService.getProductsByFilterAysnc($scope.paramFilter)
                .success(function (data) {
                
                if (data.products && data.products[0].length > 0) {
                    
                    $scope.viewProductsBinding(data.products[0]);

                    $scope.populatePaging(data.products[0][0].totalItems);
                } else {

                    $scope.products = [];

                }

            })
            .error(function (data) {
                
                console.log("data");
                console.log(data);

            });

        }
        
        $scope.pageChanged = function (newPage) {
            
            $scope.paramFilter.pageIndex = newPage;
            
            // get data
            getProducts();

        };
        
        // demo how to use watch
        $scope.$watch('brands|filter:{selected:true}', function (newVal, oldVal) {
            
            if (newVal != oldVal) {
                $scope.selectedBrands = newVal.map(function (brand) {
                    return brand.id;
                });
                
                $timeout(function () { 
                    // get products
                    getProducts();
                }, 300);   
            }
            
        }, true);

        $scope.$watch('selectedBrands', function (newVal, oldVal) {
            
            if (newVal != oldVal) {

                $scope.paramFilter.brandIds = $scope.selectedBrands;

            }

        }, true);

        $scope.$watch('pagination.itemsPerPage', function (newVal, oldVal) {
            
            if (newVal != oldVal) {
                
                $scope.paramFilter.itemsPerPage = newVal;

            }

        }, true);


    }
})();