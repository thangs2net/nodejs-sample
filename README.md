﻿# eCargo_demo_thang.letat

# Environments:
    _ Client side: Angularjs
    _ Server side: Nodejs
    _ Database : MySql

# Configuration database:
    _ In "db_scripts" folder, run script "ecargo_20160412.sql" for ecargo database schema and data.
    _ In "server" folder, change configuration to connection to database in file "mysqlConnectionString.js".

# Site structure and explanation:
    1. "Server side": 
        _ I stucture server side on "server" folder, with connection configuration and data access layer. All queries will be moved to this layer.
        _ All routes configuration base on folder "routes".
    2. "Client side":
        _ I use angularjs to do for front-end with ejs such as view engine.
        _ All files I set at "public --> javascripts --> app".
            I am trying break module smaller as I can, I thinks that useful for maintenance or reuse module.
            In my passion, I want to buildup microservices or the plugable module.
        _ I use ejs for view engine.

# Demonstration features:
    1. I tried demo how the page return 404 page not found
        cases: 404 http status 
            http://localhost:1337/product/
            http://localhost:1337/product/abc
            http://localhost:1337/product/123abc
        case product not found
            http://localhost:1337/product/100000
        case out of range
            http://localhost:1337/product/1000000000000000000000000000000
    2. Form Validation:
        Check input required and valid sample email for review form.
    3. Review form for anonmous user:
        Check if email have not existed in system, I create new user such as anonymous user.
    4. Async functions for both server side and client site.
    
# Uncommits:
    1. Performance:
        _ Should be changed query statement to store proceduzre for paging function on Filter page (index page).
    2. Visualize:
        _ Should more css for product Item, filter bar, responsive.
        _ Should add "productItem" like partial page, we will reuse and easy to makeup.
        

Many thanks for your patient.