﻿(function () {
    'user strict'

    function productRouteConfig(app) {
        
        this.app = app;
        this.routeTable = [];
        this.init();

    }
    
    productRouteConfig.prototype.init = function () {
        
        this.addRoutes();
        this.processRoutes();

    }
    
    productRouteConfig.prototype.processRoutes = function () {
        
        var self = this
        self.routeTable.forEach(function (route) {
            
            if (route.requestType === 'get') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.get(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'post') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.post(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'put') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.put(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'delete') { }

        });

    }
    
    productRouteConfig.prototype.addRoutes = function () {
        
        var self = this;
        var productDao = require('../server/Dao/productDao.js');
        
        // view product list
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/products/:take?',
            callbackFunction : function (request, response) {
                
                var take = request.params.take;
                // sync
                //productDao.getProducts(take, function (data) {
                
                //    response.render('products', {
                //        products: JSON.stringify(data)
                //    });
                
                //});
                
                productDao.getProductsAsync(take).then(function (data) {
                    
                    response.render('products', {
                        products: JSON.stringify(data)
                    });

                });
            
            }
        });
        
        // view products by brandId
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/products/:brandId/brand',
            callbackFunction : function (request, response) {
                
                var brandId = request.params.brandId;

                var regexp = /^[0-9]*$/;
                var match = brandId.match(regexp);
                
                if (brandId && match != null) { 
                
                    // sync
                    //productDao.getProductsByBrandId(brandId, function (data) {
                    
                    //    response.render('products',{
                    //        products: JSON.stringify(data),
                    //        brandId: brandId
                    //    });
                    
                    //});
                    
                    productDao.getProductsByBrandIdAsync(brandId).then(function (data) {
                        
                        response.render('products', {
                            products: JSON.stringify(data),
                            brandId: brandId
                        });

                    });

                }
                else if (match == null) {

                    response.status(404).render('view-404');

                }
                
            }
        });
        
        // get products by many brand, that use for filter function
        self.routeTable.push({
            
            requestType : 'post',
            requestUrl : '/productFilter',
            callbackFunction : function (request, response) {
                
                var paramFilter = request.body;
                
                console.log("paramFilter");
                console.log(paramFilter);

                //// sync function
                //productDao.getProductsByBrandIds(brandIds, function (data) {
                
                //    response.json({
                //        //products: JSON.stringify(data),
                //        products: data,
                //        brandIds: brandIds
                //    });
                
                //});
                
                // async
                productDao.getProductsByFilterAysnc(paramFilter).then(function (data) {
                    
                    response.json({
                        //products: JSON.stringify(data),
                        products: data,
                        //brandIds: brandIds
                    });

                });

            }
        });
        
        
        // product detail
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/product/:Id?',
            callbackFunction : function (request, response) {
                
                var id = request.params.Id;
                
                if (id == null) {

                    response.status(404).render('view-404');

                }
                
                var regexp = /^[0-9]*$/;
                var match = id.match(regexp);
                
                if (id && match != null) {
                    
                    if (isInt(id) === false) {
                        
                        response.status(404).render('view-404');
                    }
                    else {

                        // async
                        productDao.getProductByIdAsync(id).then(function (data) {
                            
                            if (data.length > 0) {
                                
                                response.render('productDetail', {
                                    products: JSON.stringify(data)
                                });

                            }
                            else {
                                
                                console.log("data omlua");
                                console.log(data);
                                
                                response.status(404).render('view-404');
                            }
                    
                        });

                    }

                }
                else if(match == null){

                    response.status(404).render('view-404');

                }

                
            }

        });

        // ============== //
        // product review //
        // ============== //
        // create brand
        self.routeTable.push({
            
            requestType : 'put',
            requestUrl : '/createReview',
            callbackFunction : function (request, response) {
                
                var review = request.body;
                
                // sync
                productDao.createReview(review, function (status) {
                    
                    response.json(status);

                });
            }

        });

        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/reviews/:productId/product',
            callbackFunction : function (request, response) {
                
                var productId = request.params.productId;
                
                var regexp = /^[0-9]*$/;
                var match = productId.match(regexp);

                if (productId && match != null) {
                    
                    // sync
                    //productDao.getProductsByBrandId(brandId, function (data) {
                    
                    //    response.render('products',{
                    //        products: JSON.stringify(data),
                    //        brandId: brandId
                    //    });
                    
                    //});
                    
                    productDao.getReviewsByProductIdAsync(productId).then(function (data) {

                        response.json( {
                            //reviews: JSON.stringify(data),
                            reviews: data,
                            productId: productId
                        });

                    });

                }
                else if (match == null) {
                    
                    response.status(404).render('view-404');

                }
                
            }
        });

        function isInteger(x) {
            return parseInt(x, 10) === x;
        }
        function isInt(value) {
            return !isNaN(value) && 
         parseInt(Number(value)) == value && 
         !isNaN(parseInt(value, 10));
        }
    }
    
    // exports
    module.exports = productRouteConfig;

})();