﻿(function () {
    'user strict'

    function brandRouteConfig(app) {
        
        this.app = app;
        this.routeTable = [];
        this.init();

    }
    
    brandRouteConfig.prototype.init = function () {
        
        //var self = this;
        
        this.addRoutes();
        this.processRoutes();
    }
    
    brandRouteConfig.prototype.processRoutes = function () {
        
        var self = this
        self.routeTable.forEach(function (route) {
            
            if (route.requestType === 'get') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.get(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'post') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.post(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'put') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.put(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'delete') { }

        });
    }
    
    brandRouteConfig.prototype.addRoutes = function () {
        
        var self = this;
        var brandDao = require('../server/Dao/brandDao.js');
        
        // view list brands
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/brands/:take?',
            callbackFunction : function (request, response) {
                
                // take is a limit number that we will take from data
                var take = request.params.take;
                
                // sync
                //brandDao.getBrands(take, function (data) {
                
                //    response.render('brands', { brands : data, title: 'Brand list' });
                
                //});
                
                // async
                brandDao.getBrandsAsync(take).then(function (data) {
                    
                    response.render('brands', { brands : data, title: 'Brand list' });

                });

            }
        });
        
        // view list brands
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/getBrands/:take',
            callbackFunction : function (request, response) {
                
                // take is a limit number that we will take from data
                var take = request.params.take;
                
                
                // this function I use sync for demo
                //brandDao.getBrands(take, function (data) {
                //    response.json({ brands : data });
                //});
                
                
                // this function I use for async
                brandDao.getBrandsAsync(take).then(function (data) {
                    
                    response.json({ brands : data });

                });
            
            }
        });
        
        // create brand
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/createBrand',
            callbackFunction : function (request, response) {
                
                response.render('createBrand', { title: 'Create new Brand' });
            }
        });
        
        // create brand
        self.routeTable.push({
            
            requestType : 'put',
            requestUrl : '/createBrand',
            callbackFunction : function (request, response) {
                
                var brand = request.body;
                
                // sync
                brandDao.createBrand(brand, function (status) {
                    
                    response.json(status);

                });

            // async
            //brandDao.createBrandAsync(brand).then( function (status) {
                
            //    response.json(status);

            //});
            }
        });

        // get api
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/getBrands/:take',
            callbackFunction : function (request, response) {
                
                // take is a limit number that we will take from data
                var take = request.params.take;
                
                
                // this function I use sync for demo
                //brandDao.getBrands(take, function (data) {
                //    response.json({ brands : data });
                //});
                
                
                // this function I use for async
                brandDao.getBrandsAsync(take).then(function (data) {
                    
                    response.json({ brands : data });

                });
            
            }
        });
    }
    
    // export
    module.exports = brandRouteConfig;
})()