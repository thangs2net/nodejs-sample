﻿(function () {
    'user strict'
    
    function userRouteConfig(app) {
        
        this.app = app;
        this.routeTable = [];
        this.init();

    }
    
    userRouteConfig.prototype.init = function () {
        
        //var self = this;
        
        this.addRoutes();
        this.processRoutes();
    }
    
    userRouteConfig.prototype.processRoutes = function () {
        
        var self = this
        self.routeTable.forEach(function (route) {
            
            if (route.requestType === 'get') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.get(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'post') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.post(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'put') {
                
                console.log("=== route ===");
                console.log(route);
                
                self.app.put(route.requestUrl, route.callbackFunction);

            }
            else if (route.requestType === 'delete') { }

        });
    }
    
    userRouteConfig.prototype.addRoutes = function () {
        
        var self = this;
        var userDao = require('../server/Dao/userDao.js');
        
        // view list users
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/users/:take?',
            callbackFunction : function (request, response) {
                
                // take is a limit number that we will take from data
                var take = request.params.take;
                
                // sync
                //userDao.getUsers(take, function (data) {
                
                //    response.render('users', { users : data, title: 'User list' });
                
                //});
                
                // async
                userDao.getUsersAsync(take).then(function (data) {
                    
                    response.render('users', { users : data, title: 'User list' });

                });

            }
        });
        
        // view list users
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/getUsers/:take',
            callbackFunction : function (request, response) {
                
                // take is a limit number that we will take from data
                var take = request.params.take;
                
                
                // this function I use sync for demo
                //userDao.getUsers(take, function (data) {
                //    response.json({ users : data });
                //});
                
                
                // this function I use for async
                userDao.getUsersAsync(take).then(function (data) {
                    
                    response.json({ users : data });

                });
            
            }
        });
        
        // create user
        self.routeTable.push({
            
            requestType : 'get',
            requestUrl : '/createUser',
            callbackFunction : function (request, response) {
                
                response.render('createUser', { title: 'Create new User' });
            }
        });
        
        // create user
        self.routeTable.push({
            
            requestType : 'put',
            requestUrl : '/createUser',
            callbackFunction : function (request, response) {
                
                var user = request.body;
                
                // sync
                userDao.createUser(user, function (status) {
                    
                    response.json(status);

                });

            // async
            //userDao.createUserAsync(user).then( function (status) {
                
            //    response.json(status);

            //});
            }
        });

        // check user
        self.routeTable.push({
            
            requestType : 'post',
            requestUrl : '/checkExistedUserByIdAsync',
            callbackFunction : function (request, response) {
                
                var user = request.body;

                // this function I use for async
                userDao.checkExistedUserByIdAsync(user.id).then(function (data) {

                    response.json(data);

                });
            
            }
        });

        self.routeTable.push({
            
            requestType : 'post',
            requestUrl : '/checkExistedUserByEmailAsync',
            callbackFunction : function (request, response) {
                
                var user = request.body;
                
                // this function I use for async
                userDao.checkExistedUserByEmailAsync(user.email).then(function (data) {
                    
                    response.json(data);

                });
            
            }
        });
    }
    
    // export
    module.exports = userRouteConfig;
})();