﻿(function () {
    'user strict'
    
    var q = require('q');
    
    var connectionProvider = require('../mysqlConnectionStringProvider.js');

    var userDao = {
    
        createUser : function (userModel, onSuccessfulCallback) {
            
            // model
            var user = {
                Username : userModel.username,
                Email : userModel.email,
                UserType: userModel.userType,
                DateOfBirth: userModel.dateOfBirth
            };
            
            var insertStatement = "insert into users set ? ";
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnection();
            
            if (connection) {
                
                console.log("if (connection)");
                console.log(user);

                connection.query(insertStatement, user, function (err, result) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnection(connection);
                    
                    if (err) {
                        console.log("=== err server ===");
                        console.log(err);
                        
                        onSuccessfulCallback({ status: 'error', errorMessage: "Insert error." });
                        //throw err;
                    }
                    
                    console.log("=== result ===");
                    console.log(result);
                    onSuccessfulCallback({ status: 'successful', result: result });

                });

            }
            else {
                
                console.log("Can not connect database.");
                onSuccessfulCallback({ status: 'error' });
            }

        }

        ,

        getUserByIdAsync : function (id) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "select * from users where id = ?";
                
                connection.query(getStatement, [id], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }
                    
                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }

        ,

        getUserByEmailAsync : function (email) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "select * from users where email = ?";
                
                connection.query(getStatement, [email], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }
                    
                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }

        ,

        checkExistedUserByIdAsync : function (id){
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "select * from users where id = ?";
                
                connection.query(getStatement, [id], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    }
                    
                    var result = {
                        status : "successful",
                        existed : true,
                        errorMessage : "",
                    };
                    
                    if (results && results.length <= 0) {
                        result.existed = false;
                    }

                    deferred.resolve(result);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }

        ,

        checkExistedUserByEmailAsync : function (email) {

            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "select * from users where email = ?";
                
                connection.query(getStatement, [email], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }
                    
                    var result = {
                        status : "successful",
                        existed : true,
                        errorMessage : "",
                        results : results
                    };
                    
                    if (results && results.length <= 0) {
                        result.existed = false;
                    }
                    
                    deferred.resolve(result);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;

        }
    }

    // exports
    module.exports = userDao;
})();