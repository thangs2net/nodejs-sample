﻿(function () {
    'user strict'
    
    var q = require('q');
    
    var connectionProvider = require('../mysqlConnectionStringProvider.js');
    
    var productDao = {
        
        getProducts : function (take, onSuccessfulCallback) {
            
            connection.query(getStatement, function (err, result, field) {
                
                //console.log("=== closedConnection(connection) ===");
                //connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                
                if (err) { throw err; }
                
                console.log("=== result ===");
                console.log(result);
                onSuccessfulCallback(result);

            });

        }
        
        ,
        
        getProductsAsync : function (take) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
        .then(function (connection) {
                
                var getStatement = "SELECT * FROM products order by id desc";
                if (take != 'undefined') {
                    
                    try {
                        if (parseInt(take) > 0) {
                            getStatement = getStatement + " limit " + [take] + " ;";
                        }
                    }
            catch (e) { }
                
                }
                
                connection.query(getStatement, function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        console.error(error);
                        deferred.reject(error);
                    }
                    deferred.resolve(results);
                });
            })
        .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }
        
        ,
        
        getProductsByBrandId : function (brandId, onSuccessfulCallback) {
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync();
            if (connection) {
                
                var getStatement = "select * from products where brandId = ? order by id desc";
                var brandIdParam = 0;
                try {
                    brandIdParam = parseInt(brandId);
                }
            catch (e) { }
                
                
                if (connection) {
                    
                    connection.query(getStatement, [brandIdParam], function (err, result, field) {
                        
                        console.log("=== closedConnection(connection) ===");
                        connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                        
                        if (err) { throw err; }
                        
                        console.log("=== result ===");
                        console.log(result);
                        onSuccessfulCallback(result);

                    });
                }
            }
        
        }
        
        ,
        
        getProductsByBrandIdAsync : function (brandId) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
        .then(function (connection) {
                
                var getStatement = "select * from products where brandId = ? order by id desc";
                var brandIdParam = 0;
                try {
                    brandIdParam = parseInt(brandId);
                }
                catch (e) { }
                
                connection.query(getStatement, [brandIdParam], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        console.error(error);
                        deferred.reject(error);
                    }
                    deferred.resolve(results);
                });
            })
        .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }
        
        ,
        
        getProductsByBrandIdsAysnc : function (brandIds) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "select * from products where brandId in ( ? )  order by id desc";
                var brandIdsParam = "0";
                
                if (brandIds.length > 0) {
                    brandIdsParam = brandIds.toString().split(",")
                }
                
                connection.query(getStatement, [brandIdsParam], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }
                    
                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }
        
        ,
        
        getProductById : function (id, onSuccessfulCallback) {
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnection();
            
            if (connection) {
                
                var getStatement = "select * from products where id = ?";
                
                if (connection) {
                    
                    console.log("=== getStatement ===");
                    console.log(getStatement);
                    
                    connection.query(getStatement, [id], function (err, result, field) {
                        
                        //console.log("=== closedConnection(connection) ===");
                        //connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                        
                        if (err) { throw err; }
                        
                        console.log("=== result ===");
                        console.log(result);
                        onSuccessfulCallback(result);

                    });
                }
            }

        }
        
        ,
        
        getProductByIdAsync : function (id) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "CALL spProductReview(?);";
                
                connection.query(getStatement, [id], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }

                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }

        ,
        getProductsByFilterAysnc : function (paramFilter) {

            var deferred = q.defer();
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnection();

            if (connection) {
                
                var getStatement = "CALL spGetProducts (?, ?, ?)";
                
                connection.query(getStatement, 
                        [paramFilter.brandIds.toString(), paramFilter.pageIndex, paramFilter.itemsPerPage], 
                        function (error, results) {
                    
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnection(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }
                    
                    deferred.resolve(results);
                });
                
            }

            return deferred.promise;
        }
        
        ,

        // ============== //
        // product review //
        // ============== //
        createReview : function (reviewModel, onSuccessfulCallback) {
            
            // model
            var review = {
                Rating : reviewModel.rating,
                Comment : reviewModel.comment,
                //CreatedDate : new Date(),
                ProductId : reviewModel.productId,
                UserId : reviewModel.userId,
            };
            
            var insertStatement = "insert into reviews set ? ";
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnection();
            
            if (connection) {
                
                connection.query(insertStatement, review, function (err, result) {
                    
                    //console.log("=== closedConnection(connection) ===");
                    //connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (err) {
                        console.log("=== err server ===");
                        console.log(err);
                        
                        onSuccessfulCallback({ status: 'error', errorMessage: "Insert error." });
                        //throw err;
                    }
                    
                    console.log("=== result ===");
                    console.log(result);
                    onSuccessfulCallback({ status: 'successful', result: result });

                });

            }
            else {
                
                console.log("Can not connect database.");
                onSuccessfulCallback({ status: 'error' });
            }
    
        }

        ,

        getReviewsByProductIdAsync : function (productId) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "select rv.id as reviewId, rv.rating, rv.comment, u.id as userId, u.username, u.email from reviews as rv join users as u on rv.userId = u.Id where productId = ? ;";

                connection.query(getStatement, [productId], function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        
                        console.error(error);
                        deferred.reject(error);
                    
                    }

                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }
    }
    
    // exports
    module.exports = productDao;

})();