﻿(function () {
    'user strict'
    
    var q = require('q');
    
    var connectionProvider = require('../mysqlConnectionStringProvider.js');
    
    var brandDao = {
        
        createBrand : function (brandModel, onSuccessfulCallback) {
            
            // model
            var brand = {
                name : brandModel.name,
                description : brandModel.description
            };
            
            var insertStatement = "insert into brands set ? ";
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnection();
            
            if (connection) {
                
                connection.query(insertStatement, brand, function (err, result) {
                    
                    //console.log("=== closedConnection(connection) ===");
                    //connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (err) {
                        console.log("=== err server ===");
                        console.log(err);
                        
                        onSuccessfulCallback({ status: 'error', errorMessage: "Insert error." });
                        //throw err;
                    }
                    
                    console.log("=== result ===");
                    console.log(result);
                    onSuccessfulCallback({ status: 'successful' });

                });

            }
            else {
                
                console.log("Can not connect database.");
                onSuccessfulCallback({ status: 'error' });
            }
    
        }
        
        ,
        
        createBrandAsync : function (brandModel, onSuccessfulCallback) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                // model
                var brand = {
                    name : brandModel.name,
                    description : brandModel.description
                };
                
                var insertStatement = "insert into brands set ? ";
                
                connection.query(insertStatement, brand, function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        console.error(error);
                        deferred.reject(error);
                    }
                    
                    console.log("=== results ===");
                    console.log(results);
                    
                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }
        
        ,
        
        getBrands : function (take, onSuccessfulCallback) {
            
            var connection = connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync();
            
            if (connection) {
                
                var getStatement = "SELECT  * FROM ecargo.brands order by id desc";
                
                if (take != 'undefined') {
                    
                    try {
                        if (parseInt(take) > 0) {
                            getStatement = getStatement + " limit " + [take] + " ;";
                        }
                    }
                catch (e) { }
                
                }
                
                connection.query(getStatement, function (err, result, field) {
                    
                    //console.log("=== closedConnection(connection) ===");
                    //connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (err) { throw err; }
                    
                    console.log("=== result ===");
                    console.log(result);
                    onSuccessfulCallback(result);

                });

            }
            else {
                
                console.log("Can not connect database.");
                onSuccessfulCallback({ status: 'error' });
            }

        }
        
        ,
        
        getBrandsAsync : function (take, onSuccessfulCallback) {
            
            var deferred = q.defer();
            
            connectionProvider.mysqlConnectionStringProvider.getMySqlConnectionAsync()
                .then(function (connection) {
                
                var getStatement = "SELECT * FROM ecargo.brands order by id desc";
                
                if (take != 'undefined') {
                    
                    try {
                        if (parseInt(take) > 0) {
                            getStatement = getStatement + " limit " + [take] + " ;";
                        }
                    }
                catch (e) { }
                }
                
                connection.query(getStatement, function (error, results) {
                    
                    console.log("=== closedConnection(connection) ===");
                    connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
                    
                    if (error) {
                        console.error(error);
                        deferred.reject(error);
                    
                    }
                    
                    console.log("=== results ===");
                    console.log(results);
                    
                    deferred.resolve(results);
                });
            })
                .fail(function (err) {
                console.error(JSON.stringify(err));
                deferred.reject(err);
                
                console.log("=== closedConnection(connection) ===");
                connectionProvider.mysqlConnectionStringProvider.closeMySqlConnectionAsync(connection);
            });
            
            return deferred.promise;
        }
    }
    
    // exports
    module.exports = brandDao;
})();