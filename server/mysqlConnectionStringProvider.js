﻿(function () {
    'user strict'
    
    //var mysql = require('mysql');
    
    //var mysql = require('mysql-promise')();
    var mysql = require('promise-mysql');
    var q = require('q');
    
    var mysqlConnectionString = require('./mySqlConnectionString.js');
    
    var mysqlConnectionStringProvider = {
        
        getMySqlConnection : function () {
            
            //var connection = mysql.createConnection(mysqlConnectionString.mysqlConnectionString.dev);
            
            
            //connection.connectAsync(function (err) { 
            
            //    if (err) { throw err; }
            
            //    console.log("Connected sucessfuly.");
            
            //});
            
            //return connection;
            
            var pool = mysql.createPool(mysqlConnectionString.mysqlConnectionString.dev);
            
            pool.getConnection(function (err, connection) {
                if (err) {
                    
                    console.log("Connect closed sucessfuly.");
                    
                    throw err;
                }
                
                console.log('connected as id ' + connection.threadId);
                
                connection.on('error', function (err) {
                    
                    if (err) {
                        
                        console.log("Connect closed sucessfuly.");
                        
                        response.status(500).render('view-500');
                        // throw err;
                    }

                });
            });
            
            return pool;

        }
        
        ,
        
        getMySqlConnectionAsync : function () {
            
            var deferred = q.defer();
            var pool = mysql.createPool(mysqlConnectionString.mysqlConnectionString.dev);
            
            pool.getConnection(function (err, connection) {
                if (err) {
                    
                    console.log("Connect closed sucessfuly.");
                    deferred.reject(err);
                    
                    throw err;
                }
                
                console.log('connected as id ' + connection.threadId);
                deferred.resolve(connection);
                
                connection.on('error', function (err) {
                    
                    if (err) {
                        
                        console.log("Connect closed sucessfuly.");
                        
                        throw err;
                    }

                });
            });
            
            return deferred.promise;

        }
        
        ,
        
        closeMySqlConnection : function (currentConnection, callback) {
            
            if (currentConnection) {
                
                currentConnection.end(function (err) {
                    
                    console.log("throw err ==> " + err);

                    if (err) {

                        callback({ errorMessage : 'Close connection have some problems.' })

                        console.log("throw err ==> " + err);
                        //throw err;
                    }
                    
                    console.log("Connect closed sucessfuly.");

                });
                console.log("throw err aaa  aa==> " );
            }

        }
        
        ,
        
        closeMySqlConnectionAsync : function (currentConnection, callback) {
            
            if (currentConnection) {
                
                //currentConnection.end(function (err) {
                    
                //    if (err) {
                        
                //        callback({ errorMessage : 'Close connection have some problems.' })
                        
                //        console.log("throw err ==> " + err);
                //        //throw err;
                //    }
                    
                //    console.log("Connect closed sucessfuly.");

                //});

                currentConnection.release(function (err) { 
                    
                    if (err) {
                        callback({ errorMessage : 'Close connection have some problems.' })

                        console.log("throw err ==> " + err);
                    }

                    console.log("Connect closed sucessfuly. release()");
                });
                console.log("Connect closed sucessfuly. release()");
            }

        }
        
        ,
        
        prepareQuery : function (query, parameters) {
            
            if (!query || !parameters) {
                throw new Error('query and parameters function parameters should be specified.');
            }
            
            return mysql.format(query, parameters);
        }
    }
    
    // exports
    module.exports.mysqlConnectionStringProvider = mysqlConnectionStringProvider;
})();