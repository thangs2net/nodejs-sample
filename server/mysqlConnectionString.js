﻿(function () {
    'user strict'

    var mysqlConnectionString = {
        
        // for development
        dev : {
            connectionLimit : 100,
            host: "localhost",
            user: "root",
            password : "123qweASD",
            database : "eCargo",
            debug    : false
        }
        
        ,
        
        // for staging
        sta : {
            connectionLimit : 100,
            host: "localhost",
            user: "root",
            password : "123qweASD",
            database : "eCargo",
            debug    : false
        }
        
        ,
        
        // for production
        pro: {
            connectionLimit : 100,
            host: "localhost",
            user: "root",
            password : "123qweASD",
            database : "eCargo",
            debug    : false
        }
    }
    
    // export
    module.exports.mysqlConnectionString = mysqlConnectionString;
})()
