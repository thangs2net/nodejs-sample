use ecargo;

SET @Array = '1,2';

SELECT p.id as productId, p.productName, p.description, p.price, p.color, p.createdDate, p.availabilityStatus, p.brandId,
		v.avgRating as rating,
        u.id as userId
FROM products as p
LEFT JOIN 
(
select ROUND (sum(rating) / count(id)) as avgRating, r.ProductId as productId, r.UserId as userId from reviews as r
) as v
	ON p.Id = v.ProductId
left join brands as b
	on b.Id = p.brandId
left outer join users as u
	ON v.UserId = u.Id

where FIND_IN_SET(p.brandId, @Array) >0
-- where p.id = 6;
-- where b.id in (1,2)