CREATE DATABASE  IF NOT EXISTS `ecargo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ecargo`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: ecargo
-- ------------------------------------------------------
-- Server version	5.5.45-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Description` text,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'first brand','1st brand'),(2,'second brand','2nd brand'),(3,'third brand','3rd brand'),(4,'fourth brand','4th brand'),(5,'fifth brand','5th brand'),(6,'',''),(8,'seventh brand','7th brand'),(9,'eighth brand','8th brand'),(10,'nineth brand','9th brand'),(11,'tenth brand','10th brand');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProductName` varchar(100) NOT NULL,
  `Description` text,
  `Price` decimal(10,0) DEFAULT NULL,
  `Color` varchar(50) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `AvailabilityStatus` tinyint(4) DEFAULT NULL,
  `BrandId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `ProductName` (`ProductName`),
  KEY `FK_product_brand` (`BrandId`),
  CONSTRAINT `FK_product_brand` FOREIGN KEY (`BrandId`) REFERENCES `brands` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Iphone 6','Apple iPhone 6 Factory Sealed Unlocked Phone, 64GB (Gold)',500,'Gold','2016-02-08 17:49:11',1,3),(2,'iPhone 6S','Apple iPhone 6S Factory Sealed Unlocked Phone, 64GB (Gold)',700,'Gold Rose','2016-01-08 09:53:54',1,3),(3,'HTC One M7','HTC One M7 Factory Unlocked Cellphone, 32GB, Silver',114,'Silver','2016-03-08 09:57:27',1,4),(4,'HTC One M9','HTC One M9 Factory Unlocked Cellphone, 32GB, Gunmetal Grey (Discontinued by Manufacturer)',531,'Grey ','2016-02-08 10:00:38',1,4),(5,'Nokia Lumia 928','Nokia Lumia 928 32GB Unlocked GSM 4G LTE Windows Smartphone w/ 8MP Carl Zeiss Optics Camera - Black',125,'Black','2016-01-08 10:03:05',1,2),(6,'Nokia 6700','Nokia 6700 Classic Gold Edition Unlocked Cell Cellular Mobile Phone EDGE and GPRS GSM',210,'Gold','2016-03-07 10:03:58',1,2),(7,'Samsung Galaxy Grand Prime','Samsung Galaxy Grand Prime DUOS 8GB Factory Unlocked GSM Smartphone -International Version- Gray',142,'Gray','2016-02-07 10:05:33',1,1),(8,'Samsung Galaxy Alpha G850a','Samsung Galaxy Alpha G850a 32GB Unlocked GSM 4G LTE Quad-Core Smartphone (Gold)',279,'Gold','2016-01-07 10:06:26',1,1),(9,'Samsung Galaxy Ace 4 Neo G318ML','Samsung Galaxy Ace 4 Neo G318ML Factory Unlocked GSM Dual-Core Android Smartphone - Black ',77,'Black','2016-03-07 10:07:14',1,1),(10,'Samsung Galaxy Star Pro DUOS S7262','Samsung Galaxy Star Pro DUOS S7262 Unlocked GSM Android 4.1 Smartphone - Black',83,'Black','2016-02-07 10:07:50',1,1),(11,'Samsung Galaxy S7 Edge - 32GB - White ','Samsung Galaxy S7 Edge G935F 32GB Factory Unlocked GSM Smartphone International Version No Warranty (White)',976,'White','2016-01-07 10:08:38',1,1),(12,'Samsung Galaxy S7 Edge - 32GB - Gold','Samsung Galaxy S7 Edge G935F 32GB Factory Unlocked GSM Smartphone International Version No Warranty (White)',976,'Gold','2016-03-07 10:08:38',1,1),(13,'Samsung Galaxy S7 Edge - 32GB - Black','Samsung Galaxy S7 Edge G935F 32GB Factory Unlocked GSM Smartphone International Version No Warranty (White)',976,'Black','2016-02-07 10:08:38',1,1),(14,'Samsung Galaxy S6 - 32GB - White','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',499,'White','2016-01-07 10:25:45',1,1),(16,'Samsung Galaxy S6 - 32GB - Black','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',499,'Black','2016-03-07 10:25:45',1,1),(17,'Samsung Galaxy S6 - 32GB - Gold','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',499,'Gold','2016-02-07 10:25:45',1,1),(18,'Samsung Galaxy S7 Edge - 64GB - White ','Samsung Galaxy S7 Edge G935F 32GB Factory Unlocked GSM Smartphone International Version No Warranty (White)',1099,'White','2016-01-07 10:08:38',1,1),(19,'Samsung Galaxy S7 Edge - 64GB - Black','Samsung Galaxy S7 Edge G935F 32GB Factory Unlocked GSM Smartphone International Version No Warranty (White)',1099,'Black','2016-03-07 10:08:38',1,1),(20,'Samsung Galaxy S7 Edge - 64GB - Gold','Samsung Galaxy S7 Edge G935F 32GB Factory Unlocked GSM Smartphone International Version No Warranty (White)',1099,'Gold','2016-02-07 10:08:38',1,1),(21,'Samsung Galaxy S6 - 64GB - White','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',599,'White','2016-01-07 10:25:45',1,1),(22,'Samsung Galaxy S6 - 128GB - White','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',699,'White','2016-03-07 10:25:45',1,1),(23,'Samsung Galaxy S6 - 64GB - Black','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',599,'Black','2016-02-07 10:25:45',1,1),(24,'Samsung Galaxy S6 - 128GB - Black','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',699,'Black','2016-01-07 10:25:45',1,1),(25,'Samsung Galaxy S6 - 64GB - Gold','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',599,'Gold','2016-03-07 10:25:45',1,1),(26,'Samsung Galaxy S6 - 128GB - Gold','Samsung Galaxy S6 Factory Unlocked 32GB Smartphone',699,'Gold','2016-02-07 10:25:45',1,1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Rating` tinyint(4) NOT NULL,
  `Comment` text,
  `CreatedDate` datetime DEFAULT NULL,
  `ProductId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_review_product` (`ProductId`),
  KEY `FK_review_user` (`UserId`),
  CONSTRAINT `FK_review_product` FOREIGN KEY (`ProductId`) REFERENCES `products` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_review_user` FOREIGN KEY (`UserId`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,5,'test',NULL,6,14),(2,5,'test',NULL,6,14),(3,5,'test',NULL,6,14),(4,5,'test',NULL,1,14),(5,8,'test',NULL,1,14),(6,9,'test',NULL,6,14),(7,10,'test',NULL,6,14);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `UserType` tinyint(4) DEFAULT NULL,
  `DateOfBirth` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Email` (`Email`),
  UNIQUE KEY `Id_UNIQUE` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (14,'anonymous','test@gmail.com',1,NULL),(15,'anonymous','joseph.thangs2@hotmail.com',1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ecargo'
--
/*!50003 DROP PROCEDURE IF EXISTS `spProductReview` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spProductReview`(
 IN productId int)
BEGIN
 
 
SELECT p.id as productId, p.productName, p.description, p.price, p.color, p.createdDate, p.availabilityStatus, p.brandId,
		v.avgRating as rating,
        u.id as userId
FROM products as p
LEFT JOIN 
(
select ROUND (sum(rating) / count(id)) as avgRating, r.ProductId as productId, r.UserId as userId from reviews as r
) as v
	ON p.Id = v.ProductId
left outer join users as u
	ON v.UserId = u.Id
where p.id = productId;


 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-13 10:51:14
