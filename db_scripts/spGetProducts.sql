CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetProducts`(
IN BrandIds    nvarchar(4000),
IN PageIndex INT,
IN ItemsPerPage INT)
BEGIN
DECLARE LowerBound INT;
DECLARE UpperBound INT;
DECLARE rownum INT;
SET LowerBound = ((PageIndex - 1) * ItemsPerPage) + 1;
SET UpperBound = ((PageIndex - 1) * ItemsPerPage) + ItemsPerPage;
set @c = (select count(id) from products);

SELECT *
  from (SELECT *, @rownum := @rownum + 1 AS rank 
		from (
-- query


SELECT p.id as productId, p.productName, p.description, p.price, p.color, p.createdDate, p.availabilityStatus, p.brandId,
		v.avgRating as rating,
        u.id as userId, @c as totalItems
        -- ,count(p.id)
FROM products as p
LEFT JOIN 
(
select ROUND (sum(rating) / count(id)) as avgRating, r.ProductId as productId, r.UserId as userId from reviews as r
) as v
	ON p.Id = v.ProductId
LEFT JOIN brands as b
	ON b.Id = p.brandId
LEFT OUTER JOIN users as u
	ON v.UserId = u.Id
WHERE FIND_IN_SET(p.brandId, @BrandIds) > 0
-- end query
		) AS d, (SELECT @rownum  := 0) r ) AS m


WHERE rank >= LowerBound and rank <= UpperBound;

END