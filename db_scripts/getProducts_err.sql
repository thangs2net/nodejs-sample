

Create PROCEDURE `spGetProducts`
(
IN BrandId    INT,
IN StartIndex INT,
IN Count INT)
BEGIN
DECLARE LowerBound INT;
DECLARE UpperBound INT;
DECLARE rownum INT;
SET LowerBound = ((StartIndex - 1) * Count) + 1;
SET UpperBound = ((StartIndex - 1) * Count) + Count;

SELECT scopeid,scopename,clientid,scope,createddate,ViewDate,IsLocked
  from (SELECT *, @rownum := @rownum + 1 AS rank 
		from (
				SELECT   sm.scopeid,sm.scopename,sm.clientid,sm.scope,sm.createddate,sm.ViewDate, sm.IsLocked
				FROM scopemaster as sm
				inner join clientmaster cm on cm.clientid=sm.clientid
				where cm.userid=ClientId order by sm.ViewDate desc
                
		) AS d, (SELECT @rownum  := 0) r ) AS m


WHERE rank >= LowerBound and rank <= UpperBound;

END