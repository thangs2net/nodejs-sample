CREATE DEFINER=`root`@`localhost` PROCEDURE `spProductReview`(
 IN productId int)
BEGIN
 
 
SELECT p.id as productId, p.productName, p.description, p.price, p.color, p.createdDate, p.availabilityStatus, p.brandId,
		v.avgRating as rating,
        u.id as userId
FROM products as p
LEFT JOIN 
(
select ROUND (sum(rating) / count(id)) as avgRating, r.ProductId as productId, r.UserId as userId from reviews as r
) as v
	ON p.Id = v.ProductId
left outer join users as u
	ON v.UserId = u.Id
where p.id = productId;


 
END